package hu.lazloz.multitenancy.config.db;

import com.zaxxer.hikari.HikariDataSource;
import hu.lazloz.multitenancy.entity.central.DbServerEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static hu.lazloz.multitenancy.config.MultiTenantManager.TENANT_DB_PREFIX;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "tenantEntityManagerFactory",
        transactionManagerRef = "tenantTransactionManager",
        basePackages = {"hu.lazloz.multitenancy.repository.tenant"})
public class TenantDatasourceConfig {

    private static final Logger log = LoggerFactory.getLogger(TenantDatasourceConfig.class);

    private final ThreadLocal<Long> currentServer = new ThreadLocal<>();
    private final ThreadLocal<Long> currentTenant = new ThreadLocal<>();
    private final Map<Object, Object> tenantDataSources = new ConcurrentHashMap<>();

    private AbstractRoutingDataSource multiTenantDataSource;

    private final DataSourceProperties properties;

    @Value("${spring.datasource.hikari.maximum-pool-size}")
    private int maximumPoolSize;

    public TenantDatasourceConfig(DataSourceProperties properties) {
        this.properties = properties;
    }

    @Bean(name = "tenantDatasource")
    public DataSource getStoreDataSource() {
        multiTenantDataSource = new AbstractRoutingDataSource() {
            @Override
            protected Object determineCurrentLookupKey() {
                return currentServer.get();
            }

            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = determineTargetDataSource().getConnection();
                connection.setCatalog(TENANT_DB_PREFIX + currentTenant.get().toString());
                return connection;
            }

            @Override
            public Connection getConnection(String username, String password) throws SQLException {
                Connection connection = determineTargetDataSource().getConnection(username, password);
                connection.setCatalog(TENANT_DB_PREFIX + currentTenant.get().toString());
                return connection;
            }
        };
        multiTenantDataSource.setTargetDataSources(tenantDataSources);
        multiTenantDataSource.afterPropertiesSet();
        return multiTenantDataSource;
    }

    @Bean(name = "tenantEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean tenantEntityManagerFactory(EntityManagerFactoryBuilder builder,
                                                                             @Qualifier("tenantDatasource") DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .packages("hu.lazloz.multitenancy.entity.tenant")
                .persistenceUnit("tenant")
                .build();
    }

    @Bean(name = "tenantJdbcNamedParameterTemplate")
    public NamedParameterJdbcTemplate tenantJdbcNamedParameterTemplateFactory(@Qualifier("tenantDatasource") DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean(name = "tenantTransactionManager")
    public PlatformTransactionManager tenantTransactionManager(@Qualifier("tenantEntityManagerFactory") EntityManagerFactory barEntityManagerFactory) {
        return new JpaTransactionManager(barEntityManagerFactory);
    }

    public void setTenant(Long serverId, Long tenantId) {
        currentServer.set(serverId);
        currentTenant.set(tenantId);
    }

    public DataSource getCurrentTenantDataSource() {
        return (DataSource) tenantDataSources.get(currentServer.get());
    }

    public void createNewDatasourceIfNotExistAndGet(DbServerEntity server) {
        log.trace("createNewDatasourceIfNotExist > serverId:{}", server.getId());
        HikariDataSource dataSource = (HikariDataSource) tenantDataSources.get(server.getId());
        if (dataSource == null) {
            dataSource = DataSourceBuilder.create()
                    .type(HikariDataSource.class)
                    .driverClassName(properties.getDriverClassName())
                    .url(server.getUrl())
                    .username(properties.getUsername())
                    .password(properties.getPassword())
                    .build();

            dataSource.setMaximumPoolSize(maximumPoolSize);

            tenantDataSources.put(server.getId(), dataSource);
            multiTenantDataSource.afterPropertiesSet();
        }
    }
}
