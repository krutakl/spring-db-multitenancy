package hu.lazloz.multitenancy.repository.tenant;

import hu.lazloz.multitenancy.entity.tenant.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
}
