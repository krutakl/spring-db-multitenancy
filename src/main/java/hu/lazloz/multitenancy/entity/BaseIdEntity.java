package hu.lazloz.multitenancy.entity;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

@MappedSuperclass
@JsonPropertyOrder({"id", "created", "creater", "lastupd", "modifier"})
public abstract class BaseIdEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "xxx_id")
    private Long id;

    @Column(name = "xxx_created")
    private ZonedDateTime created =ZonedDateTime.now();

    @Column(name = "xxx_creater")
    private String creater;

    @Column(name = "xxx_lastupd")
    private ZonedDateTime lastupd =ZonedDateTime.now();

    @Column(name = "xxx_modifier")
    private String modifier;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public ZonedDateTime getLastupd() {
        return lastupd;
    }

    public void setLastupd(ZonedDateTime lastupd) {
        this.lastupd = lastupd;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }
}
