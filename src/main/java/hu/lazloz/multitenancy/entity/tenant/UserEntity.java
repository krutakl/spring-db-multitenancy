package hu.lazloz.multitenancy.entity.tenant;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import hu.lazloz.multitenancy.entity.BaseIdEntity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "user")
@AttributeOverride(name = "id", column = @Column(name = "usr_id"))
@AttributeOverride(name = "created", column = @Column(name = "usr_created"))
@AttributeOverride(name = "creater", column = @Column(name = "usr_creater"))
@AttributeOverride(name = "lastupd", column = @Column(name = "usr_lastupd"))
@AttributeOverride(name = "modifier", column = @Column(name = "usr_modifier"))
@JsonIgnoreProperties({"modifier", "created", "creater", "lastupd"})
public class UserEntity extends BaseIdEntity {

    @Column(name = "usr_name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
