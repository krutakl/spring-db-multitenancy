create table db_server
(
    ser_id         BIGINT    NOT NULL PRIMARY KEY AUTO_INCREMENT,
    ser_name       varchar(255),
    ser_url        varchar(255),
    ser_username   VARCHAR(255),
    ser_password   varchar(255),
    ser_chooseable BOOLEAN,
    ser_created    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ser_creater    VARCHAR(50),
    ser_lastupd    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    ser_modifier   VARCHAR(50)
);

create table tenant
(
    ten_id                BIGINT    NOT NULL PRIMARY KEY AUTO_INCREMENT,
    ten_db_server_id      BIGINT,
    ten_user_id           BIGINT,
    ten_created           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ten_creater           VARCHAR(50),
    ten_lastupd           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    ten_modifier          VARCHAR(50),
    FOREIGN KEY (ten_db_server_id) REFERENCES db_server (ser_id)
);