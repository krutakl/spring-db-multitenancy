create table db_server
(
    usr_id         BIGINT    NOT NULL PRIMARY KEY AUTO_INCREMENT,
    usr_name       varchar(255),
    usr_created    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    usr_creater    VARCHAR(50),
    usr_lastupd    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    usr_modifier   VARCHAR(50)
);