package hu.lazloz.multitenancy.repository.central;

import hu.lazloz.multitenancy.entity.central.DbServerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DbServerRepository extends JpaRepository<DbServerEntity, Long> {
    List<DbServerEntity> findByChooseableTrue();
}
