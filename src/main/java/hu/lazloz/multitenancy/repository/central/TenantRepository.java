package hu.lazloz.multitenancy.repository.central;

import hu.lazloz.multitenancy.entity.central.TenantEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TenantRepository extends JpaRepository<TenantEntity, Long> {
}
