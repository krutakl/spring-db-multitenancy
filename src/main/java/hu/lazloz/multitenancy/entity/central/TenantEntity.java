package hu.lazloz.multitenancy.entity.central;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import hu.lazloz.multitenancy.entity.BaseIdEntity;

import javax.persistence.*;

@Entity
@Table(name = "tenant")
@AttributeOverride(name = "id", column = @Column(name = "ten_id"))
@AttributeOverride(name = "created", column = @Column(name = "ten_created"))
@AttributeOverride(name = "creater", column = @Column(name = "ten_creater"))
@AttributeOverride(name = "lastupd", column = @Column(name = "ten_lastupd"))
@AttributeOverride(name = "modifier", column = @Column(name = "ten_modifier"))
@JsonIgnoreProperties({"modifier", "created", "creater", "lastupd"})
public class TenantEntity extends BaseIdEntity {

    @Column(name = "ten_db_server_id")
    private Long dbServerId;

    @Column(name = "ten_user_id")
    private Long userId;

    @Column(name = "ten_client_id")
    private String clientId;

    public Long getDbServerId() {
        return dbServerId;
    }

    public void setDbServerId(Long dbServerId) {
        this.dbServerId = dbServerId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
