package hu.lazloz.multitenancy.config;

import hu.lazloz.multitenancy.repository.central.TenantRepository;
import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

import static hu.lazloz.multitenancy.config.MultiTenantManager.DEFAULT_SCHEMA;
import static hu.lazloz.multitenancy.config.MultiTenantManager.TENANT_DB_PREFIX;

@Configuration
public class FlywayConfig {
    private static final Logger log = LoggerFactory.getLogger(FlywayConfig.class);

    @Bean(name = "flyway")
    public Flyway migrateDefaultDB(@Qualifier("centralDatasource") DataSource dataSource) {
        log.info("migrateCentral");
        Flyway flyway = Flyway.configure()
                .locations("db/migration/central")
                .dataSource(dataSource)
                .schemas(DEFAULT_SCHEMA)
                .load();
        flyway.migrate();
        return flyway;
    }

    @Bean
    public Boolean migrateAllTenantDB(TenantRepository tenantRepository, MultiTenantManager multiTenantManager) {
        log.info("migrateAllTenants");
        tenantRepository.findAll().forEach(tenant -> {
            multiTenantManager.setCurrentTenant(tenant);
            migrateTenantDB(multiTenantManager.getCurrentTenantDatasource(), tenant.getId());
        });
        return true;
    }

    static void migrateTenantDB(DataSource dataSource, Long tenantId) {
        String schema = TENANT_DB_PREFIX + tenantId;
        Flyway flyway = Flyway.configure()
                .locations("db/migration/tenant")
                .dataSource(dataSource)
                .schemas(schema)
                .load();
        flyway.migrate();
    }

    static void cleanTenantDB(DataSource dataSource, Long tenantId) {
        String schema = TENANT_DB_PREFIX + tenantId;
        Flyway flyway = Flyway.configure()
                .locations("db/migration/tenant")
                .dataSource(dataSource)
                .schemas(schema)
                .load();
        flyway.clean();
    }

}
