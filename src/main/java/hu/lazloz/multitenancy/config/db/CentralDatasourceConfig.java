package hu.lazloz.multitenancy.config.db;

import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "centralEntityManagerFactory",
        transactionManagerRef = "centralTransactionManager",
        basePackages = {"hu.lazloz.multitenancy.repository.central"})
public class CentralDatasourceConfig {

    private static final Logger log = LoggerFactory.getLogger(CentralDatasourceConfig.class);

    private final DataSourceProperties properties;

    @Value("${spring.datasource.hikari.maximum-pool-size}")
    private int maximumPoolSize;

    public CentralDatasourceConfig(DataSourceProperties properties) {
        this.properties = properties;
    }

    @Primary
    @Bean(name = "centralDatasource")
    public HikariDataSource getCentralDataSource() {
        log.debug("getCentralDataSource");

        HikariDataSource defaultDataSource = DataSourceBuilder.create()
                .type(HikariDataSource.class)
                .driverClassName(properties.getDriverClassName())
                .url(properties.getUrl())
                .username(properties.getUsername())
                .password(properties.getPassword())
                .build();

        defaultDataSource.setMaximumPoolSize(maximumPoolSize);

        return defaultDataSource;
    }

    @Primary
    @Bean(name = "centralEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("centralDatasource") DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .packages("hu.lazloz.multitenancy.entity.central")
                .persistenceUnit("central")
                .build();
    }

    @Bean(name = "centralTransactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("centralEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
