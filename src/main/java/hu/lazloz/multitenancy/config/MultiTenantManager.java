package hu.lazloz.multitenancy.config;

import hu.lazloz.multitenancy.config.db.TenantDatasourceConfig;
import hu.lazloz.multitenancy.entity.central.DbServerEntity;
import hu.lazloz.multitenancy.entity.central.TenantEntity;
import hu.lazloz.multitenancy.repository.central.DbServerRepository;
import hu.lazloz.multitenancy.repository.central.TenantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.mysql.cj.jdbc.Driver;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.transaction.ChainedTransactionManager;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Configuration
public class MultiTenantManager {
    private static final Logger log = LoggerFactory.getLogger(MultiTenantManager.class);

    @Value("${multitenant.default.url}")
    private String defaultUrl;

    private final ThreadLocal<TenantEntity> currentTenant = new ThreadLocal<>();

    public static final String TENANT_DB_PREFIX = "tenant_";
    public static final String DEFAULT_SCHEMA = "central";

    private final DbServerRepository dbServerRepository;
    private final TenantRepository tenantRepository;
    private final TenantDatasourceConfig tenantDatasourceConfig;
    private final DataSourceProperties properties;

    public MultiTenantManager(DbServerRepository dbServerRepository, TenantRepository tenantRepository, TenantDatasourceConfig tenantDatasourceConfig, DataSourceProperties properties) {
        this.dbServerRepository = dbServerRepository;
        this.tenantRepository = tenantRepository;
        this.tenantDatasourceConfig = tenantDatasourceConfig;
        this.properties = properties;
    }

    @Primary
    @Bean(name = "transactionManager")
    public ChainedTransactionManager transactionManager(
            @Qualifier("centralTransactionManager") PlatformTransactionManager ds1,
            @Qualifier("tenantTransactionManager") PlatformTransactionManager ds2) {
        return new ChainedTransactionManager(ds1, ds2);
    }

    @Transactional(value = "centralTransactionManager")
    public TenantEntity setCurrentTenant(Long tenantId) {
        log.info("setCurrentTenant > tenantId:{}", tenantId);
        Optional<TenantEntity> tenantEntity = tenantRepository.findById(tenantId);
        if (tenantEntity.isPresent()) {
            return setCurrentTenant(tenantEntity.get());
        } else {
            throw new RuntimeException("Store not found tenantId: " + tenantId);
        }
    }

    @Transactional(value = "centralTransactionManager")
    public TenantEntity setCurrentTenant(TenantEntity tenantEntity) {
        log.info("setCurrentTenant > tenantId:{}", tenantEntity.getId());
        refreshDatasourceForStore(tenantEntity);
        currentTenant.set(tenantEntity);
        tenantDatasourceConfig.setTenant(tenantEntity.getDbServerId(), tenantEntity.getId());
        return tenantEntity;
    }

    private void refreshDatasourceForStore(TenantEntity tenantEntity) {
        Optional<DbServerEntity> server = dbServerRepository.findById(tenantEntity.getDbServerId());
        if (server.isPresent()) {
            tenantDatasourceConfig.createNewDatasourceIfNotExistAndGet(server.get());
        } else {
            throw new RuntimeException();
        }
    }

    @Transactional(value = "centralTransactionManager")
    public void createNewStoreDatabase(TenantEntity tenantEntity) {
        log.info("setCreateAndSetTenant");
        try {
            DbServerEntity server = getDbServer();
            tenantEntity.setDbServerId(server.getId());
            tenantRepository.save(tenantEntity);

            DataSource dataSource = new SimpleDriverDataSource(new Driver(), server.getUrl(), properties.getUsername(), properties.getPassword());
            FlywayConfig.migrateTenantDB(dataSource, tenantEntity.getId());

            tenantDatasourceConfig.createNewDatasourceIfNotExistAndGet(server);
        } catch (SQLException e) {
            log.error("An error occured in datasource removing:", e);
            throw new RuntimeException();
        }
    }

    private DbServerEntity getDbServer() {
        DbServerEntity server;
        List<DbServerEntity> chooseableServers = dbServerRepository.findByChooseableTrue();
        if (chooseableServers.size() == 0) {
            server = createDefaultDbServerIfNotExist();
        } else {
            server = chooseableServers.get(0);
        }
        if (server == null) {
            throw new RuntimeException("Not found chooseable DbServer");
        }
        return server;
    }

    private DbServerEntity createDefaultDbServerIfNotExist() {
        DbServerEntity server = null;
        List<DbServerEntity> allServer = dbServerRepository.findAll();
        if (allServer.size() == 0) {
            server = new DbServerEntity("DEFAULT", defaultUrl, true);
            dbServerRepository.save(server);
        }
        return server;
    }

    public void deleteTenant(TenantEntity tenant) {
        log.info("removeTenant");
        Long tenantId = tenant.getId();
        if (tenant.getDbServerId() != null) {
            DbServerEntity server = dbServerRepository.getOne(tenant.getDbServerId());
            try {
                tenantDatasourceConfig.setTenant(tenant.getDbServerId(), tenantId);
                DataSource dataSource = new SimpleDriverDataSource(new Driver(), server.getUrl(), properties.getUsername(), properties.getPassword());
                FlywayConfig.cleanTenantDB(dataSource, tenantId);
            } catch (SQLException e) {
                log.error("An error occured in datasource removing:", e);
                throw new RuntimeException();
            }
        }
        tenantRepository.deleteById(tenantId);
    }

    DataSource getCurrentTenantDatasource() {
        return tenantDatasourceConfig.getCurrentTenantDataSource();
    }

    public TenantEntity getCurrentTenant() {
        if (currentTenant.get() == null) {
            throw new RuntimeException();
        }
        return currentTenant.get();
    }

    public void clearCurrentTenant() {
        tenantDatasourceConfig.setTenant(null, null);
        currentTenant.set(null);
    }
}
