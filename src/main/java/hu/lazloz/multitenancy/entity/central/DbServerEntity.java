package hu.lazloz.multitenancy.entity.central;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import hu.lazloz.multitenancy.entity.BaseIdEntity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "db_server")
@AttributeOverride(name = "id", column = @Column(name = "ser_id"))
@AttributeOverride(name = "created", column = @Column(name = "ser_created"))
@AttributeOverride(name = "creater", column = @Column(name = "ser_creater"))
@AttributeOverride(name = "lastupd", column = @Column(name = "ser_lastupd"))
@AttributeOverride(name = "modifier", column = @Column(name = "ser_modifier"))
@JsonIgnoreProperties({"modifier", "created", "creater", "lastupd"})
public class DbServerEntity extends BaseIdEntity {

    @Column(name = "ser_name")
    private String name;

    @Column(name = "ser_url")
    private String url;

    @Column(name = "ser_chooseable")
    private boolean chooseable;

    public DbServerEntity() {
    }

    public DbServerEntity(String name, String url, boolean chooseable) {
        this.name = name;
        this.url = url;
        this.chooseable = chooseable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isChooseable() {
        return chooseable;
    }

    public void setChooseable(boolean chooseable) {
        this.chooseable = chooseable;
    }

}
